﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;

namespace chessPuzzleScripts
{
    class Program
    {
        Dictionary<String, long> themeMap = new Dictionary<String, long>();

        static void Main(string[] args)
        {
            Program prog = new Program();
            // prog.readPuzzleThemes();

            prog.writePuzzleSQL();

            // prog.writeThemeJsonToFile();
            Console.WriteLine("End of the World!");
            // prog.testPuzzleTest();

        }

        void writeThemeJsonToFile()
        {
            String updatedJson = JsonConvert.SerializeObject(themeMap);

            using (StreamWriter writer = new StreamWriter("updatedPuzzles.json"))
            {
                writer.WriteLine(updatedJson);
            }
        }

        void testPuzzleTest()
        {
            using (TextFieldParser parser = new TextFieldParser("testPuzzles.csv"))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");

                using (TextFieldParser updatedParser = new TextFieldParser("updatedPuzzles.csv"))
                {
                    updatedParser.TextFieldType = FieldType.Delimited;
                    updatedParser.SetDelimiters(",");

                    while (!parser.EndOfData && !updatedParser.EndOfData)
                    {
                        int fieldNum = 0;
                        //Process row
                        var fields = parser.ReadFields();
                        var updatedFileds = updatedParser.ReadFields();
                        foreach (var field in fields)
                        {
                            if (fieldNum == 7)
                            {
                                if (field.Length != testOutput(updatedFileds[fieldNum]).Length)
                                {
                                    Console.WriteLine("Error: failed to read theme properly!");
                                }
                            }
                            else if (field != updatedFileds[fieldNum])
                            {
                                Console.WriteLine("ERROR: " + field + " != " + updatedFileds[fieldNum]);
                            }
                            fieldNum++;
                        }

                    }
                }
            }
        }

        // theme is long that was ToString()ed
        String testOutput(String theme)
        {
            try
            {
                String originalTheme = "";
                // 72058693557944322
                long themeVal = long.Parse(theme);
                // Start with the largest bit value
                foreach (var item in themeMap.Reverse())
                {
                    if (item.Value <= themeVal)
                    {
                        themeVal -= item.Value;
                        String tempTheme = item.Key;
                        if (themeVal != 0)
                        {
                            tempTheme += ' ';
                        }

                        Console.Write(tempTheme);
                        originalTheme += tempTheme;

                    }
                    if (themeVal == 0)
                    {
                        Console.WriteLine();
                        Console.WriteLine("Theme test passed!");
                        break;
                    }
                }
                Console.WriteLine();
                if (themeVal != 0)
                {
                    Console.WriteLine("Test Failed: My math was off by: " + themeVal);
                }

                // original theme from testPuzzles probably out of order.
                return originalTheme;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Error: Test Failed!");
                throw;
            }

        }

        // Create puzzle themeMap
        public void readPuzzleThemes()
        {
            const String THEME_XML_FILE = "puzzleTheme.xml";
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(THEME_XML_FILE);


                System.Xml.XmlTextReader xml = new System.Xml.XmlTextReader(THEME_XML_FILE);


                long themeKey = 0;
                long total = 0;
                int themeCount = 0;
                while (xml.Read())
                {
                    if (xml.NodeType == XmlNodeType.Element && xml.HasAttributes && xml.Name == "string")
                    {
                        String theme = xml.GetAttribute(0);
                        if (!theme.Contains("Description"))
                        {
                            themeKey = ((long)1) << themeCount;
                            Console.WriteLine("Theme " + themeCount + " = " + theme + " ... key = " + themeKey);
                            this.themeMap.Add(theme, themeKey);
                            themeCount++;
                            total += themeKey;
                        }
                    }
                }

                replaceThemesColumn();

            }
            catch (System.Exception)
            {
                Console.WriteLine("Error reading file...");
                throw;
            }

        }

        public void replaceThemesColumn()
        {
            // testPuzzles.csv || lichess_db_puzzle.csv.bz2 || puzzles/lichess_db_puzzle.csv
            using (TextFieldParser parser = new TextFieldParser("testPuzzles.csv"))
            {
                // testUpdatedPuzzles.csv || updatedPuzzles.csv || updatedLichessPuzzles.csv
                using (StreamWriter writer = new StreamWriter("testUpdatedPuzzles.csv"))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Process row
                        writePuzzleLine(parser.ReadFields(), writer);
                        if (!parser.EndOfData)
                        {
                            writer.WriteLine();
                        }
                    }
                }
            }

        }


        public void writePuzzleSQL()
        {
            // testUpdatedPuzzles.csv || updatedLichessPuzzles.csv
            using (TextFieldParser parser = new TextFieldParser("updatedLichessPuzzles.csv"))
            {
                // TESTpuzzleInserts.sql || puzzleInserts.sql
                using (StreamWriter writer = new StreamWriter("puzzleInserts.sql"))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Process row
                        writePuzzleInsertLine(parser.ReadFields(), writer);

                        if (!parser.EndOfData)
                        {
                            writer.WriteLine();
                        }
                    }
                }
            }
        }




        private void writePuzzleInsertLine(string[] fields, StreamWriter writer)
        {
            if (fields.Length != 9)
            {
                Console.WriteLine("ERROR: Invalid number of fields");
                return;
            }
            // INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) (
            writer.Write("INSERT INTO chess_puzzle (id, fen, moves, rating, ratingdeviation, popularity, nbplays, themes, gameurl) (");
            int fieldNum = 0;
            foreach (string field in fields)
            {
                //TODO: Process field
                if (fieldNum == 3 || fieldNum == 4 || fieldNum == 5 || fieldNum == 6 || fieldNum == 7)
                {
                    writer.Write(field);
                }
                else if (fieldNum == 8)
                {
                    writer.WriteLine("\'" + field + "\');");
                }
                else
                {
                    writer.Write("\'" + field + "\'");
                }

                fieldNum++;
                if (fieldNum < fields.Length)
                {
                    writer.Write(",");
                }
            }

        }

        private void writePuzzleLine(string[] fields, StreamWriter writer)
        {

            int fieldNum = 0;
            foreach (string field in fields)
            {
                //TODO: Process field
                if (fieldNum == 7)
                {
                    writer.Write(calculateTheme(field));
                }
                else
                {
                    writer.Write(field);
                }
                fieldNum++;
                if (fieldNum < fields.Length)
                {
                    writer.Write(",");
                }
            }

        }

        private long calculateTheme(String themes)
        {
            long theme = 0;

            string[] themeArray = themes.Split(' ');
            foreach (string name in themeArray)
            {
                theme += this.themeMap[name];
            }

            return theme;
        }
    }


}
